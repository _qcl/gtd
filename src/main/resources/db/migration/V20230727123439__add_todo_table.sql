
drop table if exists `todo`;

create table `todo` (
    `id` int(7) not null auto_increment primary key,
    `text` varchar(255),
    `done` tinyint(1)
);