package com.ita.gtd.controller;

import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.entity.Todo;
import com.ita.gtd.service.TodoService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllList() {
        return todoService.getAllList();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody Todo todo) {
        return todoService.addTodo(todo);
    }

    @PutMapping("/{id}")
    public TodoResponse modifyTodo(@PathVariable Long id, @RequestBody Todo todo) {
        return todoService.modifyTodo(id,todo);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.getTodoById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTodo(@PathVariable Long id) {
        todoService.removeTodo(id);
    }
}
