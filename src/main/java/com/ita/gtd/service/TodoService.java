package com.ita.gtd.service;

import com.ita.gtd.advice.TodoNotFoundException;
import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.entity.Todo;
import com.ita.gtd.mapper.TodoMapper;
import com.ita.gtd.repository.TodoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> getAllList(){
        List<Todo> dbTodos = todoRepository.findAll();
        return  dbTodos.stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse addTodo(Todo todo) {
        Todo savedTodo = todoRepository.save(todo);
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(savedTodo,todoResponse);
        System.out.println(todoResponse);
        return todoResponse;
    }

    public TodoResponse modifyTodo(Long id, Todo todo) {
        Todo dbTodo = todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        dbTodo.setText(todo.getText());
        dbTodo.setDone(todo.getDone());
        return TodoMapper.toResponse(todoRepository.save(dbTodo));
    }

    public TodoResponse getTodoById(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        return TodoMapper.toResponse(todo);
    }

    public void removeTodo(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if(todo != null)
            todoRepository.deleteById(id);
    }
}
