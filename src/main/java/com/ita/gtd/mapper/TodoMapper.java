package com.ita.gtd.mapper;

import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toResponse(Todo dbTodo) {
        TodoResponse todo = new TodoResponse();
        BeanUtils.copyProperties(dbTodo, todo);
        return todo;
    }
}