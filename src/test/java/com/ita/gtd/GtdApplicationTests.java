package com.ita.gtd;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita.gtd.entity.Todo;
import com.ita.gtd.repository.TodoRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.net.http.HttpRequest;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GtdApplicationTests {

	@Autowired
	private TodoRepository todoRepository;

	@Autowired
	private MockMvc client;

	@BeforeEach
	void initRepository() {
		todoRepository.deleteAll();
	}

	@Test
	void should_return_todos_when_perform_get_given_todos_in_db() throws Exception {
		//given
		todoRepository.deleteAll();
		Todo readBook = new Todo("read 1000 books",false);
		Todo doSport = new Todo("do sport everyday",false);
		todoRepository.saveAll(List.of(readBook,doSport));

		//when then
		client.perform(MockMvcRequestBuilders.get("/todos"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(2)))
				.andExpect(jsonPath("$[0].text").value("read 1000 books"))
				.andExpect(jsonPath("$[0].done").value(false))
				.andExpect(jsonPath("$[1].text").value("do sport everyday"))
				.andExpect(jsonPath("$[1].done").value(false));
	}


	@Test
	void should_status_create_when_perform_post_given_todo() throws Exception {
		//given
		Todo readBook = new Todo("read 1000 books",false);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(readBook);
		//when then
		client.perform(MockMvcRequestBuilders.post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
				)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.text").value("read 1000 books"))
				.andExpect(jsonPath("$.done").value(false));
	}

	@Test
	void should_modify_todo_in_db_when_perform_put_given_todo() throws Exception {
		//given
		Todo readBook = new Todo("read 1000 books",false);
		Todo savedTodo = todoRepository.save(readBook);
		savedTodo.setText("read 99 books");
		savedTodo.setDone(true);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(savedTodo);

		//when then
		client.perform(MockMvcRequestBuilders.put("/todos/{id}",savedTodo.getId())
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.text").value("read 99 books"))
				.andExpect(jsonPath("$.done").value(true));
	}

	@Test
	void should_return_todo_in_db_when_perform_get_given_id() throws Exception {
		//given
		Todo readBook = new Todo("read 1000 books",false);
		Todo savedTodo = todoRepository.save(readBook);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(savedTodo);

		//when then
		client.perform(MockMvcRequestBuilders.get("/todos/{id}",savedTodo.getId())
						.contentType(MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.text").value("read 1000 books"))
				.andExpect(jsonPath("$.done").value(false));;
	}

	@Test
	void should_delete_todo_in_db_when_perform_delete_given_id() throws Exception {
		//given
		Todo readBook = new Todo("read 1000 books",false);
		Todo savedTodo = todoRepository.save(readBook);

		//when then
		client.perform(MockMvcRequestBuilders.delete("/todos/{id}",savedTodo.getId()))
				.andExpect(status().isNoContent());
		assertNull(todoRepository.findById(savedTodo.getId()).orElse(null));
	}
}
